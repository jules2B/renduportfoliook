function toggle_light_mode() {
    var app = document.getElementsByTagName("BODY")[0];
    if (localStorage.lightMode == "dark") {
        localStorage.lightMode = "light";
        app.setAttribute("light-mode", "light");
    } else {
        localStorage.lightMode = "dark";
        app.setAttribute("light-mode", "dark");
    }
}
window.addEventListener(
    "storage",
    function () {
        if (localStorage.lightMode == "dark") {
            app.setAttribute("light-mode", "dark");
        } else {
            app.setAttribute("light-mode", "light");
        }
    },
    false
);
var app = document.getElementsByTagName("BODY")[0];
if (localStorage.lightMode == "dark") {
    app.setAttribute("light-mode", "dark");
}
/*
document.addEventListener('DOMContentLoaded', () => {
    const loader = document.querySelector('#loader');
    const all = document.querySelector(".all");


    setTimeout(() => {
        loader.style.opacity = '0';
        loader.style.visibility = 'hidden';
        all.style.visibility= 'visible';
    }, 1000);
});

var loader = document.getElementById("loader")
Window.addEventListener("loader", function()
{
    loader.style.display = "none";
    all.style.visibility= 'visible';

}*/